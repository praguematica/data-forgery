# data-forgery #

***Version 1.2***

Library is used for forging mocked data. It supports simple dependencies among the fields (for generating code-name values).
It also supports post-generating operations, such as rounding, concatenating, etc.

### Quick Start ###
Install package
```
npm install data-forgery
npm test
```
Create a javascript file (myGenerator.js)
```javascript
"use strict"
var df = require("data-forgery");

var itemDefinition = {
    accountNumber: df.bank.accountNumber,                                       // Account Number
    sortCode: df.bank.sortCode,                                                 // Sort Code
    accountBalance: df.number.flt(1000, 9999),                                   // Balance between 1000 and 9999
    currency: df.other.ccy,                                                     // Currency
    exchangeRate: df.other.exchangeRate("{currency}"),                          // Exchange rate for that currency
    eligible: df.op.random(true, false)                                         // True or false randomly
}

var generated = df.forge(10, itemDefinition);                                   // Generate 10 rows
console.log(JSON.stringify(generated, null, 3));                                // Print out on screen
```
Save and run
```
node myGenerator.js
```

### About
The engine consists of 3 types of operations

* **Generators**, which are responsible for value generation

* **Operators**, which are responsible for modification of generated value (such as formatting, concatenation, multiplication of numbers, etc.)

* **Actions**, which are responsible for performing actions on the "item" level (such as deleting given property after processing)

##### For example
To generate random float number between 0 and 5, do
```javascript
var df = require("data-forgery");
var itemDefinition = {
    randomFloat: df.number.flt(0, 10)
}

```
If you want to multiply this number by 10 and then round it, do
```javascript
var df = require("data-forgery");
var itemDefinition = {
    randomFloat: df.op.round(df.op.multiply(df.number.flt(0, 5), 10))
}
```

### Argument types
There are 3 argument types which you can pass to the operators and generators
##### Raw value
Examples are 10, "abc", 0.111123, true, false, null
##### Referenced Values
```javascript
var df = require("data-forgery");
var itemDefinition = {
    myProductCode: df.bank.productCode,
    productName: df.bank.productName("{myProductCode}"),
}
```
##### Generator
You can pass another generator as a parameter
```javascript
var df = require("data-forgery");
var itemDefinition = {
    productName: df.bank.productName(df.bank.productCode),
}
```


### Generators
Currently, the following generators are available:
Generator is referenced by a full namespace, for example

*df.date.randomDate*

##### Date (df.date.*)
* randomDate(minDate, maxDate)
* randomTime(minTime, maxTime)

##### Number (df.number.*)
* flt(min, max)
* int(min, max)

##### String (df.string.*)
* firstName
* lastName
* postCode
* company
* street
* town
* address
* email
* random(length, \[stringOfPossibleCharacters\])

##### Other (df.other.*)
* ccy
* exchangeRate(targetCurrency, baseCurrency)

##### Product (df.product.*)
* productCode
* productName(productCode)
* productCategory(productCode)
* productType(productCode)
* productTypeDescription(productType)

##### Bank (df.bank.*)
* frn
* frnUk
* frnUlster
* bankName
* franchiseCode
* franchise
* accountNumber
* sortCode
* scvId
* customerType:
* customerTypeNonIndividual
* customerTypeDescription
* accountStatusCode


### Operators / Formatters ###
All operators are stored in df.op namespace.

Currently, following formatters and post processing operations are available

* op.round(numericValue)
* op.dateFormat(dateRef, format)
* op.timeFormat(timeRef, format)
* op.join(args...)
* *obsolete* op.use(valueRef) - *use op.ref instead*
* op.ref(valueRef)
* op.random(args...)
* op.sequence(args...)
* op.sequenceSpread(values, spreadAcrossItemsCount)
* oneInN(desiredValue, inHowMany, ifNotDesiredThenWhatValue)
* when(value, compareAgainst, ifTrue, ifElse)
* dictValue(key, dictionary)
* genToArray(value, generatedArraySize)
* multiply(value1, value2)
* divide(value1, value2)
* sum(args...)
* addMinutes(date, numberOfMinutesToAdd)
* substr(string, start, length)

### Actions ###
Actions don't generate or modify values. They perform some other action instead.

* action.delete(fieldName)

##### Example of action:
```javascript
var df = require("data-forgery");
var itemDefinition = {
    myProductCode: df.bank.productCode,
    productName: df.bank.productName("{myProductCode}"),


    action1: df.op.delete("myProductCode")
}
```
which will result in
```javascript
//...
{
  productName: "Current Account"
}
//...
```