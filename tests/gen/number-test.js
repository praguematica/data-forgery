"use strict";
var df = require("../../index.js");

function fltTest() {
    var val1, val2, val3, val4;
    var gen1, gen2, gen3, gen4;
    gen1 = df.number.flt(5, 15);
    gen2 = df.number.flt("{val1}", "{val2}");

    var allIntegers = true;

    var item = {val1: 10, val2: 20}

    for (var i = 0; i < 1000; i++) {
        val1 = gen1.call(item);
        val2 = gen2.call(item);
        if ((val1 < 5) || (val1 > 15)) throw "Unexpected value: " + val1;
        if ((val2 < 10) || (val2 > 20)) throw "Unexpected value: " + val2;

        // Test more dynamically
        gen3 = df.number.flt(val1, val2);
        val3 = gen3.call(item);
        if ((val3 < val1) || (val3 > val2)) throw "Unexpected value: " + val3 + " (min = " + val1 + "; max = " + val2 + ")";

        if (val2 >= val1) {
            if (val1 % 1 !== 0 || val2 % 1 !== 0 || val3 % 1 !== 0) allIntegers = false;
        }

        // test empty range
        gen4 = df.number.int(10, 5);
        val4 = gen4.call(item);
        if (val4 !== undefined) throw "Unexpected value: " + val4;

    }

    if (allIntegers) throw "Unexpected - all generated numbers were integers, at least one should be float";

    console.log(" fltTest has been successful");
}


function intTest() {
    var val1, val2, val3, val4;
    var gen1, gen2, gen3, gen4;
    gen1 = df.number.int(5, 15);
    gen2 = df.number.int("{val1}", "{val2}");

    var item = {val1: 10, val2: 20}

    for (var i = 0; i < 1000; i++) {
        val1 = gen1.call(item);
        val2 = gen2.call(item);
        if ((val1 < 5) || (val1 > 15)) throw "Unexpected value: " + val1;
        if ((val2 < 10) || (val2 > 20)) throw "Unexpected value: " + val2;

        // Test more dynamically
        gen3 = df.number.int(val1, val2);
        val3 = gen3.call(item);
        if ((val3 < val1) || (val3 > val2)) throw "Unexpected value: " + val3 + " (min = " + val1 + "; max = " + val2 + ")";

        if (val2 >= val1) {
            if (val1 % 1 !== 0) throw "Unexpected value: " + val1;
            if (val2 % 1 !== 0) throw "Unexpected value: " + val2;
            if (val3 % 1 !== 0) throw "Unexpected value: " + val3;
        }
    }

    console.log(" intTest has been successful");
}

module.exports = {
    run: function () {
        console.log("** Testing df.number module");
        fltTest();
        intTest();
    }
}
