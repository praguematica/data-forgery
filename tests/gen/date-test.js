"use strict";
var df = require("../../index.js");

function testRandomDate() {
    var value1, value2;
    var gen1 = df.date.randomDate(new Date(2016, 0, 1), new Date(2016, 3, 1));
    var gen2 = df.date.randomDate("{dateFrom}", "{dateTo}");

    for (var i = 0; i < 1000; i++) {
        value1 = gen1.call(this);
        value2 = gen2.call({
            dateFrom: new Date(2015, 0, 1),
            dateTo: new Date(2015, 3, 1)
        });

        // Test that there is no time
        if (value1.getHours() !== 0 || value1.getMinutes() !== 0
            || value1.getSeconds() !== 0 || value1.getMilliseconds() !== 0) {
            throw "Unexpected value: " + value1;
        }

        if (value2.getHours() !== 0 || value2.getMinutes() !== 0
            || value2.getSeconds() !== 0 || value2.getMilliseconds() !== 0) {
            throw "Unexpected value: " + value2;
        }

        // Test that it falls into the given range
        
        if (value1.getTime() > (new Date(2016, 3, 1).getTime())) throw "Unexpected value: " + value1;
        if (value1.getTime() < (new Date(2016, 0, 1).getTime())) throw "Unexpected value: " + value1;
        if (value2.getTime() > (new Date(2015, 3, 1).getTime())) throw "Unexpected value: " + value1;
        if (value2.getTime() < (new Date(2015, 0, 1).getTime())) throw "Unexpected value: " + value1;

    }
    console.log(" testRandomDate successful")
}

function testRandomTime() {
    var value1, value2;
    var gen1 = df.date.randomTime(new Date(2016, 0, 1), new Date(2016, 3, 1));
    var gen2 = df.date.randomTime("{dateFrom}", "{dateTo}");

    for (var i = 0; i < 1000; i++) {
        value1 = gen1.call(this);
        value2 = gen2.call({
            dateFrom: new Date(2015, 0, 1),
            dateTo: new Date(2015, 3, 1)
        });

        // Test that there is some time
        if (!(value1.getHours() !== 0 || value1.getMinutes() !== 0
            || value1.getSeconds() !== 0 || value1.getMilliseconds() !== 0)) {
            throw "Unexpected value: " + value1;
        }

        if (!(value2.getHours() !== 0 || value2.getMinutes() !== 0
            || value2.getSeconds() !== 0 || value2.getMilliseconds() !== 0)) {
            throw "Unexpected value: " + value2;
        }

        // Test it falls into given range
        if (value1.getTime() > (new Date(2016, 3, 1).getTime())) throw "Unexpected value: " + value1;
        if (value1.getTime() < (new Date(2016, 0, 1).getTime())) throw "Unexpected value: " + value1;
        if (value2.getTime() > (new Date(2015, 3, 1).getTime())) throw "Unexpected value: " + value1;
        if (value2.getTime() < (new Date(2015, 0, 1).getTime())) throw "Unexpected value: " + value1;

    }

    console.log(" testRandomTime successful");
}

module.exports = {
    run: function() {
        console.log("** Testing df.date module");
        testRandomDate();
        testRandomTime();
    }
}
