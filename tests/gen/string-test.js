"use strict";
var df = require("../../index.js");

function testFirstName() {
    var gen, val;
    gen = df.string.firstName;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testFirstName successful");
}

function testLastName() {
    var gen, val;
    gen = df.string.lastName;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testLastName successful");
}

function testPostCode() {
    var gen, val;
    gen = df.string.postCode;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testPostCode successful");
}

function testCompany() {
    var gen, val;
    gen = df.string.company;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testCompany successful");
}

function testStreet() {
    var gen, val;
    gen = df.string.street;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testStreet successful");
}

function testTown() {
    var gen, val;
    gen = df.string.town;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testTown successful");
}

function testAddress() {
    var gen, val;
    gen = df.string.address;
    for (var i = 0; i < 1000; i++) {
        val = gen.call(this);
        if (isEmpty(val)) throw "Unexpected empty value: " + val;
    }
    console.log(" testAddress successful");
}

function testEmail() {
    var gen1, gen2, val1, val2;
    var item;

    gen1 = df.string.email("Mick", "Jagger");
    gen2 = df.string.email("{firstName}", "{lastName}");

    item = {
        firstName: "Hillary",
        lastName: "Clinton",
        company: "White House"
    }

    for (var i = 0; i < 1000; i++) {
        val1 = gen1.call(this);
        val2 = gen2.call(item);

        if (isEmpty(val1)) throw "Unexpected empty value: " + val1;
        if (isEmpty(val2)) throw "Unexpected empty value: " + val2;

        if (val1.indexOf("mick") === -1 || val1.indexOf("jagger") === -1) throw "Unexpected value: " + val1;
        if (val2.indexOf("hillary") === -1 || val2.indexOf("clinton") === -1) throw "Unexpected value: " + val2;

        // Test valid email (we don't need to test complicated emails here
        var re = /[^\s@]+@[^\s@]+\.[^\s@]+/;
        if (!re.test(val1)) throw "Unexpected: email not in the expected format: " + val1;
        if (!re.test(val2)) throw "Unexpected: email not in the expected format: " + val2;

    }
    console.log(" testEmail successful");
}

function testRandom() {
    // Test operators
    var res = df.string.random;
    res.call(this);
    if (!res) throw ("Unexpected result: " + res);

    console.log(" testRandom has been successful");
}

function isEmpty(value) {
    return (value === undefined || value === null || value === "");
}

module.exports = {
    run: function() {
        console.log("** Testing df.string module");
        testFirstName();
        testLastName();
        testPostCode();
        testCompany();
        testStreet();
        testTown();
        testAddress();
        testEmail();
        testRandom();
    }
}