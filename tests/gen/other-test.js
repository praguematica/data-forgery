"use strict";
var df = require("../../index.js");

function testCcy() {
    var gen1, val1;
    gen1 = df.other.ccy;

    for (var i = 0; i < 1000; i++) {
        val1 = gen1.call(this);
        if (!val1 || val1 == "") throw "Unexpected value: " + val1;
    }

    console.log(" testCcy successful");
}

function testExchangeRate() {
    var gen1, gen2, gen3, gen4, gen5;
    var val1, val2, val3, val4, val5;

    var item = {ccy1: "EUR", ccy2: "GBP"};

    gen1 = df.other.exchangeRate("EUR");
    gen2 = df.other.exchangeRate("GBP");
    gen3 = df.other.exchangeRate("EUR", "GBP");
    gen4 = df.other.exchangeRate("GBP", "EUR");
    gen5 = df.other.exchangeRate("EUR", "EUR");

    for (var i = 0; i < 1000; i++) {
        val1 = gen1.call(item);
        val2 = gen2.call(item);
        val3 = gen3.call(item);
        val4 = gen4.call(item);
        val5 = gen5.call(item);

        if (!val1 || val1 === "" || val1 === 0) throw "Unexpected value: " + val1;
        if (!val2 || val2 === "" || val1 === 0) throw "Unexpected value: " + val2;
        if (!val3 || val3 === "" || val1 === 0) throw "Unexpected value: " + val3;
        if (!val4 || val4 === "" || val1 === 0) throw "Unexpected value: " + val4;
        if (!val5 || val5 === "" || val1 === 0) throw "Unexpected value: " + val5;

        if (val1 === 1) throw "Unexpected value: " + val1;
        if (val2 !== 1) throw "Unexpected value: " + val2;
        if (val3 === 1) throw "Unexpected value: " + val3;
        if (val4 === 1) throw "Unexpected value: " + val4;
        if (val5 !== 1) throw "Unexpected value: " + val5;
    }
    console.log(" testExchangeRate successful");
}

module.exports = {
    run: function() {
        console.log("** Testing df.other module");
        testCcy();
        testExchangeRate();
    }
}
