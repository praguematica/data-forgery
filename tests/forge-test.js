"use strict";
var df = require("../index.js");

function testForge() {
    var def = {
        firmRegNumber: df.bank.frn,
        franchiseCode: df.bank.franchiseCode,
        franchise: df.bank.franchise("{franchiseCode}"),
        franchiseCodeDep: df.bank.franchiseCode("{firmRegNumber}"),

        accountNumber: df.bank.accountNumber,
        sortCode: df.bank.sortCode,
        accountBalance: df.number.flt(10000000, 999999999),
        currency: df.other.ccy,
        exchangeRate: df.other.exchangeRate("{currency}"),

        customerName: df.op.random(df.op.join(df.string.firstName, " ", df.string.lastName), df.string.company),
        accountStatusCode: df.bank.accountStatusCode,
        eligible: df.op.random(true, false),
        street: df.string.street,
        int1: df.number.int(0, 100),
        int2: df.number.int,
        town: df.string.town,
        phone: df.other.phone,
        email: df.string.email,

        randomAlphanumeric: df.string.random,
        randomAlphanumeric2: df.string.random(10, "ABC")
    }

    var generated = df.forge(10, def);
    for (var i = 0; i < generated.length; i++) {
        var item = generated[i];
        for (var key in def) {
            if (item[key] === null || item[key] === undefined || item[key] === "" ) throw "Item at position " + i + ": field " + key + " is null or undefined";
        }
    }


    generated = df.forge([{someVar: "someValue0"},{someVar: "someValue1"}], def);

    if (!generated || !generated.length || generated.length !== 2) {
        throw ("There has been a problem generating records: Expected two, got " + (generated || []).length);
    }

// Check contents
    for (var i = 0; i < generated.length; i++) {
        var item = generated[i];
        for (var key in def) {
            if (item[key] === null || item[key] === undefined || item[key] === "" ) throw "Item at position " + i + ": field " + key + " is null or undefined";
        }
        // Check someVar
        if (item.someVar !== ("someValue"+i)) {
            throw ("Item " + i + "; someVar value = " + item.someVar + "; expected someValue" + i + ";");
        }
    }

    console.log(" testForge successful");
}

module.exports = {
    run: function() {
        console.log("** Testing df.forge")
        testForge();
    }
}