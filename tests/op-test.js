var df = require("../index.js");
var moment = require("moment");

function testRound() {
    var gen1 = df.number.flt(5.54546, 5.99999);
    var gen2 = df.number.flt("{inValue1}", "{inValue2}");
    var item = {inValue1: 4.54546, inValue2: 4.999999};
    var val;

    val = df.op.round.call(item, 10.4323).call(item);
    if (val !== 10) throw "Unexpected value: " + val;

    val = df.op.round.call(item, "{inValue1}").call(item);
    if (val !== 5) throw "Unexpected value: " + val;

    val = df.op.round.call(item, gen1).call(item);
    if (val !== 6) throw "Unexpected value: " + val;

    val = df.op.round.call(item, gen2).call(item);
    if (val !== 5) throw "Unexpected value: " + val;

    val = df.op.round.call(item, "Some String Rubbish").call(item);
    if (!isNaN(val)) throw "Unexpected value: " + val;

    console.log(" testRound successful");
}

function testDateFormat() {
    var gen1 = df.date.randomDate(new Date(2016, 3, 3), new Date(2016, 3, 3));
    var gen2 = df.number.flt("{inValue1}", "{inValue2}");
    var item = {inValue1: moment(new Date(2016, 4, 4)), inValue2: moment(new Date(2016, 4, 4))};
    var val;

    val = df.op.dateFormat.call(item, new Date(2016, 0, 1)).call(item);
    if (val !== "20160101") throw "Unexpected value: " + val;

    val = df.op.dateFormat.call(item, new Date(2016, 1, 1), "YYYY-MM-DD").call(item);
    if (val !== "2016-02-01") throw "Unexpected value: " + val;

    val = df.op.dateFormat.call(item, gen1).call(item);
    if (val !== "20160403") throw "Unexpected value: " + val;

    val = df.op.dateFormat.call(item, gen2).call(item);
    if (val !== "20160504") throw "Unexpected value: " + val;

    console.log(" testDateFormat successful");
}

function testTimeFormat() {
    var gen1 = df.date.randomDate(new Date(2016, 3, 3), new Date(2016, 3, 3));
    var gen2 = df.number.flt("{inValue1}", "{inValue2}");
    var item = {inValue1: moment(new Date(2016, 4, 4)), inValue2: moment(new Date(2016, 4, 4))};
    var val;

    val = df.op.dateTimeFormat.call(item, new Date(2016, 0, 1)).call(item);
    if (val !== "20160101000000") throw "Unexpected value: " + val;

    val = df.op.dateTimeFormat.call(item, new Date(2016, 1, 1), "YYYY-MM-DD hh:mm:ss").call(item);
    if (val !== "2016-02-01 12:00:00") throw "Unexpected value: " + val;

    val = df.op.dateTimeFormat.call(item, gen1).call(item);
    if (val !== "20160403000000") throw "Unexpected value: " + val;

    val = df.op.dateTimeFormat.call(item, gen2).call(item);
    if (val !== "20160504000000") throw "Unexpected value: " + val;

    console.log(" testDateTimeFormat successful");

}

function testJoin() {

}

function testWhen() {
    // Test operators
    var gItem = {inValue: 10};
    var res = null;

    res = df.op.when("{inValue}", 10, true, false).call(gItem);
    if (res !== true) throw ("Unexpected result. Expected true, got " + res);

    res = df.op.when("{inValue}", 11, true, false).call(gItem);
    if (res !== false) throw ("Unexpected result. Expected false, got " + res);

    res = df.op.when("some value", "some value", 1000, 100).call(gItem);
    if (res !== 1000) throw ("Unexpected result. Expected 1000, got " + res);

    res = df.op.when("some other value", "some value", 1000, 100).call(gItem);
    if (res !== 100) throw ("Unexpected result. Expected 100, got " + res);

    // test boolean
    res = df.op.when("{inValue}", true, true, false).call({inValue: true});
    if (res !== true) throw ("Unexpected result. Expected true, got " + res);

    res = df.op.when("{inValue}", false, true, false).call({inValue: true});
    if (res !== false) throw ("Unexpected result. Expected false, got " + res);

    res = df.op.when("{inValue}", false, true, false).call({inValue: false});
    if (res !== true) throw ("Unexpected result. Expected true, got " + res);

    res = df.op.when("{inValue}", true, true, false).call({inValue: false});
    if (res !== false) throw ("Unexpected result. Expected false, got " + res);


    console.log(" testWhen successful");
}

function testDictValue() {
    var val;
    var item = {inValue: "DEF"}
    var dict = {
        ABC: "Some Value 1",
        DEF: "Some Value 2"
    }

    val = df.op.dictValue("ABC", dict).call(item);
    if (val !== "Some Value 1") throw "Unexpected value: " + val;

    val = df.op.dictValue("DEF", dict).call(item);
    if (val !== "Some Value 2") throw "Unexpected value: " + val;

    val = df.op.dictValue("WHATEVER", dict).call(item);
    if (val !== undefined) throw "Unexpected value: " + val;

    // Test referenced value
    val = df.op.dictValue("{inValue}", dict).call(item);
    if (val !== "Some Value 2") throw "Unexpected value: " + val;

    // Test generator as an input
    val = df.op.dictValue(df.op.ref("{inValue}"), dict).call(item);
    if (val !== "Some Value 2") throw "Unexpected value: " + val;

    console.log(" testDictValue successful");
}

function testSequence() {
    // Test operators
    var def = null;
    var res = null;

    def = df.op.sequence(5, 10, 15);

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5");

    res = def.call(this, {});
    if (res !== 10) throw ("Unexpected result. Expected 10");

    res = def.call(this, {});
    if (res !== 15) throw ("Unexpected result. Expected 15");

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5");

    res = def.call(this, {});
    if (res !== 10) throw ("Unexpected result. Expected 10");

    console.log(" testSequence successful");
}

function testSequenceSpread() {
    // Test operators
    var def = null;
    var res = null;

    def = df.op.sequenceSpread([5, 10, 15], 2);

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5");

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5");

    res = def.call(this, {});
    if (res !== 10) throw ("Unexpected result. Expected 10");

    res = def.call(this, {});
    if (res !== 10) throw ("Unexpected result. Expected 10");

    res = def.call(this, {});
    if (res !== 15) throw ("Unexpected result. Expected 15");

    res = def.call(this, {});
    if (res !== 15) throw ("Unexpected result. Expected 15");

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5, got " + res);

    res = def.call(this, {});
    if (res !== 5) throw ("Unexpected result. Expected 5");

    res = def.call(this, {});
    if (res !== 10) throw ("Unexpected result. Expected 10");

    console.log(" testSequenceSpread successful");
}

function testOneInN() {
    var gen1 = df.number.int(10,10);
    var gen2 = df.number.flt("{inValue1}", "{inValue2}");
    var item = {inValue1: 15, inValue2: 3, inValue3: 20};

    var val;

    // Test that it works for plain values
    val = df.op.oneInN(true, 2, false).call(item);
    if (val !== true && val !== false) throw "Unexpected value: " + val;

    // Test it works with strings
    val = df.op.oneInN("ABC", 2, "DEF").call(item);
    if (val !== "ABC" && val !== "DEF") throw "Unexpected value: " + val;

    // Test it works with numbers
    val = df.op.oneInN(4, 2, 8).call(item);
    if (val !== 4 && val !== 8) throw "Unexpected value: " + val;

    // Test it works with referenced value
    val = df.op.oneInN("{inValue1}", "{inValue2}", "{inValue2}").call(item);
    if (val !== 15 && inValue !== 20) throw "Unexpected value: " + val;

    // test if it works with the generators
    val = df.op.oneInN(df.number.int(25, 25), df.number.int(2, 2), df.number.int(30, 30)).call(item);
    if (val !== 25 && val !== 30) throw "Unexpected value: " + val;

    // test if it works with generators which reference values
    val = df.op.oneInN(df.number.int("{inValue1}", "{inValue1}"), df.number.int("{inValue2}", "{inValue2}"), df.number.int("{inValue3}", "{inValue3}")).call(item);
    if (val !== 15 && val !== 20) throw "Unexpected value: " + val;


    // Test probability results
    var trueCount = 0;
    var falseCount = 0;
    for (var i = 0; i < 1000; i ++) {
        val = df.op.oneInN(true, 5, false).call(null);
        if (val === true) {
            trueCount++;
        } else if (val === false) {
            falseCount++
        } else {
            throw "Unexpected value: " + val;
        }
    }
    var ratio = falseCount / (trueCount || 1);
    if (ratio < 3 || ratio > 5) {
        throw("oneInN deviates a little too much:  true count = " + trueCount + "; falseCount = " + falseCount + " = " + (falseCount / trueCount));
    }

    console.log(" testOneInN successful");
}

function testGenToArray() {
    // Test operators
    var params = null;
    var res = null;

    params = df.op.genToArray(df.number.int(20, 30), 5);
    if (!params || params.length != 5) {
        throw "Not getting array or the length does not match";
    }

    for (var i = 0; i < params.length; i++) {
        if (!params[i] || params[i] < 20 | params[i] > 30) {
            throw "Unexpected value on position " + i + ": " + params[i];
        }
    }
    console.log(" testGenToArray successful");
}

function testRef() {
    // Test operators
    var gItem1 = {inValue: 10};
    var gItem2 = {inValue: "Something"};
    var gItem3 = {};
    var res = null;
    var def = df.op.ref("{inValue}");

    res = def.call(gItem1);
    if (res !== 10) throw "Unexpected value: " + res;

    res = def.call(gItem2);
    if (res !== "Something") throw "Unexpected value: " + res;

    res = def.call(gItem3);
    if (res !== undefined) throw "Unexpected value: " + res;

    console.log(" testUse successful");
}

function testRandom() {
    var item = {
        val1: "ABC",
        val2: "DEF",
        val3: "GHI",
        val4: "JKL"
    };

    var pool = ["ABC", "DEF", "GHI", "JKL"];
    var val;

    for (var i = 0; i < 1000; i++) {
        // test using parameters, not array
        val = df.op.random("ABC", "DEF", "GHI", "JKL").call(item);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;

        // test using array
        val = df.op.random(pool).call(item);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;

        // test using referenced value
        val = df.op.random("{val1}", "{val2}", "{val3}", "{val4}").call(item);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;

        // test using generators - that it returns generator
        val = df.op.random(df.op.ref("{val1}"), df.op.ref("{val2}"), df.op.ref("{val3}"), df.op.ref("{val4}")).call(item);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;
    }

    console.log(" testRandom successful");
}

function testMultiply() {
    var gItem1 = {inValue1: 10, inValue2: 20};
    var res = null;
    var def = null;

    def = df.op.multiply(5, 6);
    res = def.call(gItem1);
    if (res !== 30) throw "Unexpected value: " + res;

    def = df.op.multiply(0, 1);
    res = def.call(gItem1);
    if (res !== 0) throw "Unexpected value: " + res;

    def = df.op.multiply(1, 0);
    res = def.call(gItem1);
    if (res !== 0) throw "Unexpected value: " + res;

    def = df.op.multiply("something", 0);
    res = def.call(gItem1);
    if (res !== null) throw "Unexpected value: " + res;

    def = df.op.multiply(0, {});
    res = def.call(gItem1);
    if (res !== null) throw "Unexpected value: " + res;

    def = df.op.multiply(0, []);
    res = def.call(gItem1);
    if (res !== null) throw "Unexpected value: " + res;

    def = df.op.multiply(3, "{inValue2}");
    res = def.call(gItem1);
    if (res !== 60) throw "Unexpected value: " + res;

    def = df.op.multiply("{inValue1}", 6);
    res = def.call(gItem1);
    if (res !== 60) throw "Unexpected value: " + res;

    def = df.op.multiply("{inValue1}", "{inValue2}");
    res = def.call(gItem1);
    if (res !== 200) throw "Unexpected value: " + res;

    console.log(" testMultiply successful");
}

function testDivide() {
    var gItem1 = {inValue1: 10, inValue2: 20};
    var res = null;
    var def = null;

    def = df.op.divide(10, 10);
    res = def.call(gItem1);
    if (res !== 1) throw "Unexpected value: " + res;

    def = df.op.divide(10, 5);
    res = def.call(gItem1);
    if (res !== 2) throw "Unexpected value: " + res;

    def = df.op.divide(7, 2);
    res = def.call(gItem1);
    if (res !== 3.5) throw "Unexpected value: " + res;

    def = df.op.divide(10, 0);
    res = def.call(gItem1);
    if (res !== null) throw "Unexpected value: " + res;

    def = df.op.divide(1, 5);
    res = def.call(gItem1);
    if (res !== 0.2) throw "Unexpected value: " + res;

    def = df.op.divide("{inValue1}", "{inValue2}");
    res = def.call(gItem1);
    if (res !== 0.5) throw "Unexpected value: " + res;

    def = df.op.divide("{inValue2}", "{inValue1}");
    res = def.call(gItem1);
    if (res !== 2) throw "Unexpected value: " + res;

    console.log(" testDivide successful");

}

function testAddMinutes() {
    var value = new Date();
    var valueObject = {
        fromDate: value
    }

    var res = df.op.addMinutes(value, 5).call(this);
    if (!res) throw "Unexpected";
    if((res.getTime() - value.getTime()) !== 300000) throw "Unexpected " + (res.getTime() - value.getTime());


    var res = df.op.addMinutes(value, -10).call(this)
    if (!res) throw "Unexpected";
    if((res.getTime() - value.getTime()) !== -600000) throw "Unexpected " + (res.getTime() - value.getTime());

    var res = df.op.addMinutes(value, 5).call(this);
    if (!res) throw "Unexpected";
    if((res.getTime() - value.getTime()) !== 300000) throw "Unexpected " + (res.getTime() - value.getTime());

    var res = df.op.addMinutes("{fromDate}", 5).call(valueObject);
    if (!res) throw "Unexpected";
    if((res.getTime() - value.getTime()) !== 300000) throw "Unexpected";

    console.log(" testAddMinutes successful");

}

function testSum() {
    var def;
    var res;
    var valueObject = {
        val1: 100,
        val2: 50,
        val3: undefined,
        val4: null,
        val5: "abc",
        val6: 5,
        val8: 1
    }

    def = df.op.sum(10, 20, 5);
    res = def.call(this)
    if (res != 35) throw ("Unexpected result: " + res);

    def = df.op.sum(10, -5, 3);
    res = def.call(this)
    if (res != 8) throw ("Unexpected result: " + res);

    def = df.op.sum(0.1, 0.5);
    res = def.call(this)
    if (res != 0.6) throw ("Unexpected result: " + res);

    def = df.op.sum();
    res = def.call(this)
    if (res != 0) throw ("Unexpected result: " + res);

    def = df.op.sum(10, "abc", null, 5);
    res = def.call(this)
    if (res != 15) throw ("Unexpected result: " + res);

    def = df.op.sum("{val1}", "{val2}", "{val3}", "{val4}", "{val5}", "{val6}", "{val7}", "{val8}");
    res = def.call(valueObject)
    if (res != 156) throw ("Unexpected result: " + res);

    console.log(" testSum successful");

}

function testSubstr() {
    var def;
    var res;
    var valueObject = {
        val1: "ABCDEF"
    }

    def = df.op.substr("QWERTYUIOP", 0, 3);
    res = def.call(this)
    if (res != "QWE") throw ("Unexpected result: " + res);

    def = df.op.substr("QWERTYUIOP", 1, 4);
    res = def.call(this)
    if (res != "WERT") throw ("Unexpected result: " + res);

    def = df.op.substr("QWERTYUIOP", 3);
    res = def.call(this)
    if (res != "RTYUIOP") throw ("Unexpected result: " + res);

    def = df.op.substr("{val1}", 0, 3);
    res = def.call(valueObject)
    if (res != "ABC") throw ("Unexpected result: " + res);

    def = df.op.substr("{val1}", 1, 4);
    res = def.call(valueObject)
    if (res != "BCDE") throw ("Unexpected result: " + res);

    def = df.op.substr("{val1}", 3);
    res = def.call(valueObject);
    if (res != "DEF") throw ("Unexpected result: " + res);

    console.log(" testSubstr successful");
}


module.exports = {
    run: function() {
        console.log("** Testing df.op module");
        testRound();
        testDateFormat();
        testTimeFormat();
        testJoin();
        testRef();
        testRandom();
        testSequence();
        testSequenceSpread();
        testOneInN();
        testWhen();
        testDictValue();
        testGenToArray();
        testMultiply();
        testDivide();
        testSum();
        testAddMinutes();
        testSubstr();
    }

}