var dfInternals = require("../df/df.js");
var df = require("../index.js");

function testGetRandomKey() {
    var val;
    var keyPool = {ABC: 1, DEF: 2, GHI: 3, JKL: 4}

    for (var i = 0; i < 1000; i++) {
        val = dfInternals.getRandomKey(keyPool);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;
    }
    console.log(" testGetRandomKey successful");
}

function testGetRandomElement() {
    var val;
    var elementPool = ["ABC", "DEF", "GHI", "JKL"];
    for (var i = 0; i < 1000; i++) {
        val = dfInternals.getRandomElement(elementPool);
        if (val !== "ABC" && val !== "DEF" && val !== "GHI" && val !== "JKL") throw "Unexpected value: " + val;
    }
    console.log(" testGetRandomElement successful");
}

function testGetReferencedValue() {
    var val;
    var item = {
        var1: "Variable 1",
        var2: 12,
        var3: true,
        var4: -1000,
        var5: 1.105
    }
    
    val = dfInternals.getReferencedValue.call(item, "{var1}");
    if (val !== "Variable 1") throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{var2}");
    if (val !== 12) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{var3}");
    if (val !== true) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{var4}");
    if (val !== -1000) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{var5}");
    if (val !== 1.105) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{someNotDefined}",  item);
    if (val !== undefined) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{}");
    if (val !== undefined) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "{broken");
    if (val !== null) throw "Unexpected value: " + val;

    val = dfInternals.getReferencedValue.call(item, "someotherrubbish");
    if (val !== null) throw "Unexpected value: " + val;

    console.log(" testGetReferencedValue successful");
}

function testGetParameterValue() {
    var val;
    var item = { var1: "Variable 1", currency: "GBP" };
    var gen1 = df.string.firstName;
    var gen2 = df.other.exchangeRate("GBP");
    var gen3 = df.other.exchangeRate("{currency}");

    // Test if works with strings
    val = dfInternals.getParameterValue.call(item, "someStringValue");
    if (val !== "someStringValue") throw "Unexpected value: " + val;

    // test if it works with numbers
    val = dfInternals.getParameterValue.call(item, 12);
    if (val !== 12) throw "Unexpected value: " + val;

    // test if it works with referenced values
    val = dfInternals.getParameterValue.call(item, "{var1}");
    if (val !== "Variable 1") throw "Unexpected value: " + val;

    // test if it returns the referenced id if the referenced value does not exist
    val = dfInternals.getParameterValue.call(item, "{nonExisting}");
    if (val !== "{nonExisting}") throw "Unexpected value: " + val;

    // test if it works with generator - simple
    val = dfInternals.getParameterValue.call(item, gen1);
    if (!val) throw "Unexpected value: " + val;

    // test if it works with generator - which uses static value
    val = dfInternals.getParameterValue.call(item, gen2);
    if (val !== 1) throw "Unexpected value: " + val;

    // test if it works with generator - which uses referenced value
    val = dfInternals.getParameterValue.call(item, gen3);
    if (val !== 1) throw "Unexpected value: " + val;

    console.log(" testGetParameterValue successful");
}

function testGetRandomNumber() {
    var val;

    for (var i = 0; i < 1000; i++) {
        val = dfInternals.getRandomNumber(0, 1);
        if (val < 0 || val > 1) throw "Unexpected value: " + val;

        val = dfInternals.getRandomNumber(10, 20);
        if (val < 10 || val > 20) throw "Unexpected value: " + val;

        val = dfInternals.getRandomNumber(-5, 5);
        if (val < -5 || val > 5) throw "Unexpected value: " + val;

        var caught = false;
        try {
            val = dfInternals.getRandomNumber(10, 0);
        } catch (err) {
            caught = true;
        }
        if (!caught) throw "Unexpected: min was > max but exception has not been thrown";
    }
    console.log(" testGetRandomNumber successful");
}

function testGetRandomChars() {
    var val;

    for (var i = 0; i < 1000; i++) {
        val = dfInternals.getRandomChars(10);
        if (val.length !== 10) throw "Unexpected length, should be 10: " + val;

        val = dfInternals.getRandomChars(5);
        if (val.length !== 5) throw "Unexpected length, should be 5: " + val;

        val = dfInternals.getRandomChars(0);
        if (val.length !== 0) throw "Unexpected length, should be 0: " + val;

        val = dfInternals.getRandomChars(50, "QWERTY");
        if (val.length !== 50) throw "Unexpected length, should be 50: " + val;
        var regex = /^[QWERTY]*$/;
        if (!regex.test(val)) throw "Unexpected characters in the string: " + val;
    }
    console.log(" testGetRandomChars successful");

}

module.exports = {
    run: function() {
        console.log("** Testing df internal methods");
        testGetRandomKey();
        testGetRandomElement();
        testGetReferencedValue();
        testGetParameterValue();
        testGetRandomNumber();
        testGetRandomChars();
    }

}