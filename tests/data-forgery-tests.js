"use strict"
var df = require("../index.js");

// Internal methods test
var dfTest = require("./df-tests");

// Generator tests
var dateTest = require("./gen/date-test");
var numberTest = require("./gen/number-test")
var stringTest = require("./gen/string-test");
var otherTest = require("./gen/other-test")

// Operator test
var opTest = require("./op-test.js");

// Forge test
var forgeTest = require("./forge-test");

// Run tests
dfTest.run();

dateTest.run();
numberTest.run();
stringTest.run();
otherTest.run();

opTest.run();

forgeTest.run();




