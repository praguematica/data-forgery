"use strict"
var util = require("./df/util.js");
var op = require("./df/op.js");
var other = require("./df/other.js");
var bank = require("./df/bank.js");
var product = require("./df/product.js");
var string = require("./df/string.js");
var date = require("./df/date.js");
var number = require("./df/number.js");
var factory = require("./df/factory.js");

function forge(arrKeysOrNum, defs) {
    var result = [];
    var numRecords = 0;
    if (util.isArray(arrKeysOrNum)) {
        numRecords = arrKeysOrNum.length;
    } else if (!isNaN(arrKeysOrNum)) {
        numRecords = arrKeysOrNum;
    }

    for (var i = 0; i < numRecords; i++) {
        var item = {};

        if (util.isArray(arrKeysOrNum)) {
            var fldItem = arrKeysOrNum[i];
            var fldItemKeys = Object.keys(fldItem);
            for (var k = 0; k < fldItemKeys.length; k++) {
                var valueAs = fldItemKeys[k];
                var value = fldItem[valueAs];
                item[valueAs] = value;
            }
        }

        for (var def in defs) {
            var fieldId = def;
            var fieldValue;
            var fieldValueDef = defs[def];
            if (util.isFunction(fieldValueDef)) {
                fieldValue = fieldValueDef.call(item);
            } else {
                fieldValue = fieldValueDef;
            }
            item[fieldId] = fieldValue;
        }

        result.push(item);
    }
    return result;

}


// Exports
module.exports = {
    other: other,
    bank: bank,
    product: product,
    string: string,
    number: number,
    date: date,

    op: op,
    util: util,
    factory: factory,
    forge: forge
}


