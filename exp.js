"use strict";

var getReferencedValue = function(referencedValue) {
    try {
        var fieldValue = null;
        var fldRef = referencedValue.match(new RegExp("{" + "(.*)" + "}"));
        if (fldRef && fldRef.length > 1) {
            var fldRefName = fldRef[1];
            var fldRefValue = this[fldRefName];
            return fldRefValue;
        }
        return fieldValue;
    } catch (err) {
        return referencedValue;
    }
}


var GenFactory = function() {
    this.instance = function() {
        return new Generator();
    }
}


var NumberGenerator = function () {
    this.multiply = function (param) {
        this._register(function () {
            this.value = this.value * param;
        });
        return this;
    }

    this.random = function () {
        this._register(function () {
            this.value = Math.random() * 100;
        });
        return this;
    }
}

var StringGenerator = function () {
    this.round = function () {
        this._register(function () {
            this.value = Math.round(this.value);
        });
        return this;
    }

    this.concat = function (reference) {
        this._register(function () {
            var refValue = getReferencedValue.call(this.context, reference);
            this.value = this.value + refValue;
        });
        return this;
    }
}


var Generator = function () {
    this.seq = [];
    this.context = {};

    this._register = function (generator) {
        this.seq.push(generator);
    }

    this.set = function(reference) {
        this._register(function(){
            this.value = getReferencedValue.call(this.context, reference);
        })
        return this;
    }

    this.toString = function () {
        this.value = "" + this.value;
        return this;
    }

    this.generate = function() {
        for (var i = 0; i < this.seq.length; i++) {
            this.seq[i].call(this);
        }
        return this.value;
    }

    this.setContext = function(context) {
        this.context = context;
    }

};


Generator.prototype.register = function (name, fn) {
    this[name] = fn;
}

NumberGenerator.call(Generator.prototype);
StringGenerator.call(Generator.prototype);

function registerGenerator(name, fn) {
    Generator.prototype[name] = fn;
}

registerGenerator("add", function (param) {
    this._register(function () {
        this.value += param;
    });
    return this;
})


var gf = new GenFactory();
var gen = function() {
    return new Generator();
}


var def = {
    val1: 20,
    genValue1: gen().set(10).multiply(10).round().add(20).multiply(0.1).round().toString().concat("{val1}"),
    genValue2: gen().random().multiply(10).round().add(20).multiply(0.1).round().toString().concat(20),
    genValue3: gen().set("{val1}").multiply(10).round().add(20).multiply(0.1).round().toString().concat(20)
}

var item = {};
for (var prop in def) {
    var generatedValue = null;
    if (def[prop] instanceof Generator) {
        def[prop].setContext(item);
        generatedValue = def[prop].generate();
    } else {
        generatedValue = def[prop];
    }
    item[prop] = generatedValue;
}

console.log(JSON.stringify(item, null, 3));