"use strict"
var df = require("./df.js");
var factory = require("./factory.js");

var franchiseCode = function (reference) {
    if (reference) {
        return function () {
            // This is to return Ulster based on FRN = ULSTER
            var refValue = df.getReferencedValue.call(this, reference);
            return df.getRandomKey(refValue == "FRNULSTER" ? franchiseDictionaryUlster : franchiseDictionaryUK);
        }
    }
    return df.getRandomKey(franchiseDictionary);
}

// Dictionaries
var frnDictionaryUk = {
    FRNCOUTTS: "Coutts & Co.",
    FRNRBS: "RBS",
    FRNADAMCO: "Adam & Co.",
    FRNNATWEST: "National Westminster Bank"
}

var frnDictionaryUlster = {
    FRNULSTER: "Ulster Bank"
}

var frnDictionary = {}
for (var attrname in frnDictionaryUk) { frnDictionary[attrname] = frnDictionaryUk[attrname]; }
for (var attrname in frnDictionaryUlster) { frnDictionary[attrname] = frnDictionaryUlster[attrname]; }

var franchiseDictionaryUK = {
    CBP: "C & PB",
    CIB: "C & IB",
    PBB: "P & BB",
    WEALTH: "Wealth"
}

var franchiseDictionaryUlster = {
    ULSTERNORTH: "Ulster North",
    ULSTERSOUTH: "Ulster South"
}

// Merge both
var franchiseDictionary = {}
for (var attrname in franchiseDictionaryUK) { franchiseDictionary[attrname] = franchiseDictionaryUK[attrname]; }
for (var attrname in franchiseDictionaryUlster) { franchiseDictionary[attrname] = franchiseDictionaryUlster[attrname]; }

var customerTypeDictionary = {
    SLA: "Small local Authority",
    PLAuth: "Public authority",
    PPS: "Personal Pension Scheme",
    SPS: "Stakeholder Pension Scheme",
    OPS: "Occupational Pension Scheme",
    SSA: "Small self-administered Pension Scheme",
    Pen: "Pension Scheme",
    Ret: "Retirement Funds",
    CIV: "Collective Investment Scheme",
    InvF: "Investment Firm",
    Fin: "Financial Institution",
    Credit: "Credit institution",
    cLarge: "Large Company",
    SME: "Micro, Small & Medium-sized enterprise",
    NP: "Non-Profit Institutions serving households",
    Indv: "Individual"
}

var customerTypeNonIndividual = {
    SLA: "Small local Authorities (budget < 500k euros)",
    PLAuth: "Public authorities (excluding small-local authorities with a budget < 500k euros)",
    InvF: "Investment Firms",
    Fin: "Financial Institutions (Including Insurance undertakings and reinsurance undertakings)",
    Credit: "Credit institutions (banks, building societies and credit unions",
    cLarge: "Large company",
    SME: "Micro, Small & Medium-sized enterprise"
}

var customerTypeEligibilityDictionary = {
    SLA: true,
    PLAuth: true,
    PPS: true,
    SPS: true,
    OPS: true,
    SSA: true,
    Pen: true,
    Ret: true,
    Credit: false,
    Fin: false,
    InvF: false,
    CIV: false,
    cLarge: true,
    SME: true,
    NP: true,
    Indv: true
}

var accountStatusDictionary = {
    CTI: "Ineligible Customer",
    noIDV: "No valid ID&V",
    EAA: "Branch outiside EEA",
    PTI: "Ineligible Product",
    STP: "Eligible - CB further investigation needed",
    DEC: "Deceased Customer",
    GA: "Customers who moved address",
    HMTS: "Depositors on Sanctions list",
    LEGDIS: "Formal Notice, Legal or competing claim",
    BRP: "Deposit holder bancrupt",
    MLO: "Suspiscion of money laundering",
    BLK: "Any other block",
    FRD: "Suspiscion of fraudulent activity",
    BEN: "Trust, client or child trust account",
    LDttDAP: "Dormant account - legally",
    IntDor: "Dorman account - internally",
    NA24: "No activity in 24 months",
    LEGDOR: "Dormant legally - not transferred",
    ColRec: "Subject to collection or recovery proceedings",
    CashStop: "Cashline Stop placed",
    SPCOM: "Special needs customer",
    ExDEC: "Deceased Customer, executor appointed"
}

var accountStatusDictionaryLong = {
    CTI: "Customer Type is Ineligible",
    noIDV: "Customer has no valid Identification and Validation on record",
    EAA: "Otherwise eligible account, but branch is outside EEA",
    PTI: "Product Type is ineligible",
    STP: "Eligible deposits that the authorised credit institution has deemed fit for straight through pay-out. These are eligible deposits where no further investigation is required by the Central Bank before a compensation payment is made.",
    DEC: "Where the depositor is confirmed deceased and no executor has been appointed",
    GA: "Customers who have moved address and have not notified the financial institution; and the institution failed to contact them for an extended period of time",
    HMTS: "Depositors that appear on the International Financial Sanctions list. These are sanctions that are restrictive measures imposed by the EU or the UN on individuals or entities in an effort to curtail their activities and to exert pressure and influence on them.",
    LEGDIS: "Account for which the financial institution has received formal notice of a legal or competing claims to the proceeds of the account.",
    BRP: "Deposit holder is declared bankrupt.",
    MLO: "Deposit flagged for a suspicion of money laundering offence.",
    BLK: "Any other block has been placed on deposit that prevents the depositor from withdrawing funds.",
    FRD: "A block on the account as a result of fraudulent activity/transactions and hence suspended by the authorised credit institution.",
    BEN: "Accounts for the following types of depositor accounts 1) trust accounts, 2) client accounts, 3) child trust accounts,",
    LDttDAP: "Account is Legally Dormant Account and transferred to Dormant Account fund operator",
    IntDor: "Account is internally classified as Inactive or Dormant, but not yet legally dormant",
    NA24: "Account does not have any activity in last 24 months",
    LEGDOR: "Account is Legally Dormant and not transferred to Dormant Account Provider",
    ColRec: "Account and or holder(s) subject to collection or recovery proceedings",
    CashStop: "Cashline Stop placed on Account",
    SPCOM: "Special Communication requirements of the customer (like Braille)",
    ExDEC: "Where the depositor is confirmed deceased and executor has been appointed"
}

var accountStatusEligibilityDictionary = {
    CTI: "No",
    noIDV: "No",
    EAA: "No",
    PTI: "No",
    STP: "Yes",
    DEC: "Yes",
    GA: "Yes",
    HMTS: "Potential",
    LEGDIS: "Potential",
    BRP: "Yes",
    MLO: "Yes",
    BLK: "Yes",
    FRD: "Yes",
    BEN: "Potential",
    LDttDAP: "No",
    IntDor: "Yes",
    NA24: "Yes",
    LEGDOR: "Potential",
    ColRec: "Yes",
    CashStop: "Yes",
    SPCOM: "Yes",
    ExDEC: "Yes"
}

var accountStatusFFSTPDictionary = {
    CTI: false,
    noIDV: false,
    EAA: false,
    PTI: false,
    STP: true,
    DEC: false,
    GA: false,
    HMTS: false,
    LEGDIS: false,
    BRP: false,
    MLO: false,
    BLK: false,
    FRD: false,
    BEN: false,
    LDttDAP: false,
    IntDor: false,
    NA24: false,
    LEGDOR: false,
    ColRec: false,
    CashStop: false,
    SPCOM: false,
    ExDEC: true,
}

// Exports
module.exports = {
    frn: factory.randomKey(frnDictionary),
    frnUk: factory.randomKey(frnDictionaryUk),
    frnUlster: factory.randomKey(frnDictionaryUlster),
    bankName: factory.referencedValue(frnDictionary),
    franchiseCode: franchiseCode,
    franchise: factory.referencedValue(franchiseDictionary),
    accountNumber: function() {return df.getRandomNumber(10000000, 99999999)},
    sortCode: function() {return df.getRandomNumber(100000, 999999)},
    scvId: function() {return df.getRandomNumber(100000000000, 999999999999)},
    customerType: factory.randomKey(customerTypeDictionary),
    customerTypeNonIndividual: factory.randomKey(customerTypeNonIndividual),
    customerTypeDescription: factory.referencedValue(customerTypeDictionary),
    customerTypeEligibility: factory.referencedValue(customerTypeEligibilityDictionary),
    accountStatusCode: factory.randomKey(accountStatusDictionary),
    accountStatusDescription: factory.referencedValue(accountStatusDictionary),
    accountStatusEligibility: factory.referencedValue(accountStatusEligibilityDictionary),
    accountStatusFFSTP: factory.referencedValue(accountStatusFFSTPDictionary),

    data: {
        frns: Object.keys(frnDictionary),
        franchises: Object.keys(franchiseDictionary),
        customerTypes: Object.keys(customerTypeDictionary),
        accountStatusCodes: Object.keys(accountStatusDictionary)
    }
}
