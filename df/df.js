"use strict"
var util = require("./util.js");

var getRandomKey = function(assocArray) {
    var keys = Object.keys(assocArray);
    var randomKey = keys[Math.floor(keys.length * Math.random())];
    return randomKey;
}

var getRandomElement = function(array) {
    array = array || [];
    var rndm =  array[Math.floor(Math.random() * array.length)];
    return rndm;
}

var getReferencedValue = function(referencedValue) {
    try {
        var fieldValue = null;
        var fldRef = referencedValue.match(new RegExp("{" + "(.*)" + "}"));
        if (fldRef && fldRef.length > 1) {
            var fldRefName = fldRef[1];
            var fldRefValue = this[fldRefName];
            return fldRefValue;
        }
        return fieldValue;
    } catch (err) {
        return referencedValue;
    }
}

var getParameterValue = function(parameter) {
    var result = null;
    if (util.isFunction(parameter)) {
        result = parameter.call(this);
    } else {
        // Try to get referenced value
        result = getReferencedValue.call(this, parameter);
        if (result === null || result === undefined) {
            // Use static value instead
            result = parameter;
        }
    }
    return result;
}

var getRandomNumber = function(min, max) {
    if (min > max) {
        throw "Min: " + min + " is > than Max: " + max;
    }
    min = min || 0;
    max = max || 1;
    return Math.floor(Math.random() * (max - min) + min);
}

var getRandomChars = function (length, pool) {
    var text = "";
    var possible = pool || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

module.exports = {
    getRandomKey: getRandomKey,
    getRandomElement: getRandomElement,
    getReferencedValue: getReferencedValue,
    getParameterValue : getParameterValue,
    getRandomNumber: getRandomNumber,
    getRandomChars: getRandomChars,
}