"use strict"
var df = require("./df.js");
var flt = function (minRef, maxRef) {
    if (minRef != undefined) {
        return function() {
            // Try if it is a referenced value
            var min = df.getReferencedValue.call(this, minRef);
            var max = df.getReferencedValue.call(this, maxRef);
            max = max || min * 10;
            if (min > max) {
                return undefined;
            }
            return (Math.random() * (max - min) + min)
        }
    }
    return (Math.random() * (1 - 0) + 0);
}

var int = function(minRef, maxRef) {
    return function() {
        if (minRef > maxRef) {
            return undefined;
        }
        var val = (flt.call(this, minRef || 0, maxRef || 100).call(this));
        return Math.round(val);
    }
}

module.exports = {
    flt: flt,
    int: int
}