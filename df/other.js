"use strict"
var util = require("./util.js");
var df = require("./df.js");
var number = require("./number.js");

var ccy = function() {
    return df.getRandomKey(currencies)
}

var exchangeRate = function(curRef, baseCurrency) {
    baseCurrency = baseCurrency || "GBP";
    if (curRef) {
        return function() {
            var currency = df.getParameterValue.call(this, curRef);
            return currencies[baseCurrency][currency];
        }
    }
    return currencies[baseCurrency][ccy()]
}

module.exports = {
    ccy: ccy,
    exchangeRate: exchangeRate,
    phone: function(){
        return "0" + number.int(20, 150).call(this) + "" + number.int(10000000,99999999).call(this);
    }
}


var currencies = {
    "GBP": {
        "GBP": 1.0000,
        "EUR": 0.790402
    },
    "EUR":{
        "GBP": 1.271,
        "EUR": 1
    },

}
