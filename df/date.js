"use strict"
var df = require("./df.js");

function getRandomTime(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function randomDate(startRef, endRef) {
    return function() {
        var start = df.getParameterValue.call(this, startRef) || new Date();
        var end = df.getParameterValue.call(this, endRef) || new Date();
        var randomTime = getRandomTime(start, end);
        randomTime.setHours(0);
        randomTime.setMinutes(0);
        randomTime.setSeconds(0);
        randomTime.setMilliseconds(0);
        return randomTime;
    }
}

function randomTime(startRef, endRef) {
    return function() {
        var start = df.getParameterValue.call(this, startRef) || new Date();
        var end = df.getParameterValue.call(this, endRef) || new Date();
        return getRandomTime(start, end);
    }
}

module.exports = {
    randomDate: randomDate,
    randomTime: randomTime
}
