"use strict"
var moment = require("moment");
var df = require("./df.js");
var util = require("./util.js");

var formatDateTime = function (dateRef, timeFormat) {
    return function () {
        if ((timeFormat || "") === "") {
            throw "no time format provided";
        }
        var dateValue = df.getParameterValue.call(this, dateRef);
        return moment(dateValue).format(timeFormat || "YYYYMMDDHHmmss");
    }
}

module.exports = {
    round: function () {
        var args = arguments;
        return function () {
            var result = df.getParameterValue.call(this, args[0]);
            var precision = (args.length > 1) ? args[1] : 0;
            return Math.round(result * Math.pow(10, precision)) / Math.pow(10, precision);
        }
    },

    dateFormat: function (dateRef, dateFormat) {
        return formatDateTime(dateRef, dateFormat || "YYYYMMDD");
    },

    dateTimeFormat: function (dateTimeRef, dateTimeFormat) {
        return formatDateTime(dateTimeRef, dateTimeFormat || "YYYYMMDDHHmmss");
    },

    join: function () {
        var args = arguments;
        return function () {
            // Run all submethods
            var resultPool = [];
            for (var i = 0; i < args.length; i++) {
                var result = df.getParameterValue.call(this, args[i]);
                resultPool.push(result);
            }
            // Join the results
            return resultPool.join("");
        }
    },

    /* Simply copies the value */
    use: function (valueRef) {
        console.warn("op.use now obsolete, please use op.ref instead");
        return function () {
            var value = df.getReferencedValue.call(this, valueRef);
            return value;
        }
    },

    ref: function (valueRef) {
        return function () {
            var value = df.getReferencedValue.call(this, valueRef);
            return value;
        }
    },


    random: function () {
        var args = util.isArray((arguments || [])[0]) ? arguments[0] : arguments;
        return function () {
            var rParameter = args[Math.floor(Math.random() * args.length)];
            var result = df.getParameterValue.call(this, rParameter);
            return result;
        }
    },

    sequence: function () {
        var args = util.isArray((arguments || [])[0]) ? arguments[0] : arguments;
        if (!args || args.length == 0) return null;

        var counterItemIndex = 0;
        return function () {
            var rParameter = args[counterItemIndex];
            var result = df.getParameterValue.call(this, rParameter);
            counterItemIndex++;
            counterItemIndex = Math.min(counterItemIndex % args.length);
            return result;
        }
    },

    sequenceSpread: function (args, spreadAcross) {
        var args = util.isArray((arguments || [])[0]) ? arguments[0] : arguments;
        if (!args || args.length == 0) return null;

        var counterItemIndex = 0;
        var counterSpreadIndex = 0;
        return function () {
            counterSpreadIndex = Math.floor(counterItemIndex / spreadAcross);
            if (counterSpreadIndex >= args.length) {
                // Reset
                counterItemIndex = 0; // reset
                counterSpreadIndex = Math.floor(counterItemIndex / spreadAcross);
            }

            var rParameter = args[counterSpreadIndex];
            var result = df.getParameterValue.call(this, rParameter);
            counterItemIndex++;
            return result;
        }
    },


    oneInN: function (oneValueRef, inCount, elseValue) {
        var pool = [null]; // Will be replaced
        for (var i = 1; i < inCount; i++) {
            pool.push(elseValue)
        }
        return function () {
            var pickIndex = Math.floor(Math.random() * pool.length);
            if (pickIndex == 0) {
                return df.getParameterValue.call(this, oneValueRef);
            }
            return df.getParameterValue.call(this, pool[pickIndex]);
        }
    },

    when: function (inParameter, inConditionValue, outParameterPositive, outParameterNegative) {
        return function () {
            var inParameterValue = df.getParameterValue.call(this, inParameter);
            if (inParameterValue === inConditionValue) {
                return df.getParameterValue.call(this, outParameterPositive);
            } else {
                return df.getParameterValue.call(this, outParameterNegative)
            }
        }
    },

    dictValue: function (refValue, refDictionary) {
        return function () {
            var value = df.getParameterValue.call(this, refValue);
            var dictionary = df.getParameterValue.call(this, refDictionary);
            return (dictionary || {})[value];
        }
    },

    /* Used for arguments like spread, spreadAcross, etc. */
    genToArray: function (generator, arrLength) {
        var that = this;
        var result = [];
        for (var i = 0; i < arrLength; i++) {
            var value = df.getParameterValue.call(this, generator);
            result.push(value);
        }
        return result;
    },

    multiply: function (ref1, ref2) {
        return function () {
            var val1 = df.getParameterValue.call(this, ref1);
            var val2 = df.getParameterValue.call(this, ref2);
            if (util.isArray(val1) || util.isArray(val2) || isNaN(val1) || isNaN(val2)) {
                return null;
            }
            return val1 * val2;
        }
    },

    divide: function (ref1, ref2) {
        return function () {
            var val1 = df.getParameterValue.call(this, ref1);
            var val2 = df.getParameterValue.call(this, ref2);
            if (util.isArray(val1) || util.isArray(val2) || isNaN(val1) || isNaN(val2) || val2 === 0) {
                return null;
            }
            return val1 / val2;
        }
    },

    sum: function () {
        var args = arguments;
        return function () {
            // Run all submethods
            var result = 0;
            for (var i = 0; i < args.length; i++) {
                var paramValue = df.getParameterValue.call(this, args[i]);
                result += (paramValue !== null && paramValue !== undefined & !isNaN(paramValue)) ? paramValue : 0;
            }
            return result;
        }
    },

    addMinutes: function (valueRef, numMinutesRef) {
        return function () {
            var value = df.getParameterValue.call(this, valueRef);
            var numMinutes = df.getParameterValue.call(this, numMinutesRef) || 0;
            var result = null;
            if (value && value.getTime) {
                result = new Date(value.getTime() + numMinutes * 60000);
            }
            return result;
        }
    },

    substr: function (ref1, start, length) {
        return function () {
            var val1 = df.getParameterValue.call(this, ref1);
            if (typeof val1 === "string") {
                return val1.substr(start, length)
            }
            return null;
        }
    },

    action: {
        delete: function(fieldReference) {
            return function() {
                delete this[fieldReference];
            }
        }
    }

}