"use strict"
var df = require("./df.js");
module.exports = {
    referencedValue: function (dictionary) {
        return function (reference) {
            if (reference) {
                return function () {
                    return dictionary[df.getReferencedValue.call(this, reference)];
                }
            }
        }
    },

    randomKey: function(dictionary) {
        return function () {
            return df.getRandomKey(dictionary);
        }
    },
}