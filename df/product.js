"use strict"
var df = require("./df.js");

var productCode = function () {
    return df.getRandomKey(productCodes);
}


var productName = function (reference) {
    return function () {
        return productCodes[df.getParameterValue.call(this, reference)];
    }
}

var productCategory = function (reference) {
    return function () {
        return productCategories[df.getParameterValue.call(this, reference)];
    }
}

var productType = function (reference) {
    return function () {
        return productTypes[df.getParameterValue.call(this, reference)];
    }
}

var productTypeDescription = function (reference) {
    return function () {
        return productTypeDictionary[df.getParameterValue.call(this, reference)];
    }
}

var praPriority = function(reference) {
    return function() {
        return praPriorityDictionary[df.getParameterValue.call(this, reference)];
    }
}

var productCodes = {
    "DL_042": "Advantage Business Account",
    "DL_039": "Bonus Saver",
    "DL_040": "Business Current Account",
    "DL_044": "Business Plus Account",
    "DL_027": "Business Reserve",
    "DL_026": "CBFM Business Current Account",
    "DL_017": "Client Deposit Manager",
    "DL_016": "Client Deposit Service",
    "DL_014": "Clients' Monies Services",
    "DL_015": "Clients' Monies Services for Solicitors",
    "DL_004": "Corporate Cash Manager",
    "DL_005": "Corporate Cash Manager Plus",
    "DL_006": "Corporate Cash Manager Plus (S8)",
    "DL_046": "Direct Business Bank Account",
    "DL_028": "Direct Reserve",
    "DL_011": "Enhanced Fixed Rate Deposit",
    "DL_041": "Foundation Account",
    "DEPO-IRS": "IPED Deposit Plan (Where RBS is Plan Manager)",
    "DL_003": "Liquidity Manager",
    "DL_008": "Liquidity Manager 30 Day Notice Account",
    "DL_010": "Liquidity Manager 95 Day Notice Account",
    "DL_001": "Liquidity Select",
    "DL_007": "Liquidity Select 30 Day Notice Account",
    "DL_009": "Liquidity Select 95 Day Notice Account",
    "DL_043": "Royalties Business Account"
}

var productCategories = {
    "DL_042": "Money Transmission Account",
    "DL_039": "Deposit & Liquidity",
    "DL_040": "Money Transmission Account",
    "DL_044": "Money Transmission Account",
    "DL_027": "Deposit & Liquidity",
    "DL_026": "Money Transmission Account",
    "DL_017": "Deposit & Liquidity",
    "DL_016": "Deposit & Liquidity",
    "DL_014": "Deposit & Liquidity",
    "DL_015": "Deposit & Liquidity",
    "DL_004": "Deposit & Liquidity",
    "DL_005": "Deposit & Liquidity",
    "DL_006": "Deposit & Liquidity",
    "DL_046": "Money Transmission Account",
    "DL_028": "Deposit & Liquidity",
    "DL_011": "Deposit & Liquidity",
    "DL_041": "Money Transmission Account",
    "DEPO-IRS": "Structured term deposit all tenors",
    "DL_003": "Deposit & Liquidity",
    "DL_008": "Deposit & Liquidity",
    "DL_010": "Deposit & Liquidity",
    "DL_001": "Deposit & Liquidity",
    "DL_007": "Deposit & Liquidity",
    "DL_009": "Deposit & Liquidity",
    "DL_043": "Money Transmission Account"
}


var productTypeDictionary = {
    IAA: "Instant Access Accounts",
    ISA: "ISAs",
    NA: "Notice accounts",
    FD1: "Fixed term deposits < 1y",
    FD2: "Fixed term deposits < 2y",
    FD4: "Fixed term deposits < 4y",
    FP4P: "Fixed term deposits > 4y",
    Other: "Other"
}

var productTypes = {
    "DL_042": "IAA",
    "DL_039": "IAA",
    "DL_040": "IAA",
    "DL_044": "ISA",
    "DL_027": "ISA",
    "DL_026": "ISA",
    "DL_017": "Other",
    "DL_016": "Other",
    "DL_014": "Other",
    "DL_015": "IAA",
    "DL_004": "IAA",
    "DL_005": "FP4P",
    "DL_006": "FP4P",
    "DL_046": "FD4",
    "DL_028": "FD1",
    "DL_011": "FD1",
    "DL_041": "FD2",
    "DEPO-IRS": "FD2",
    "DL_003": "FD4",
    "DL_008": "NA",
    "DL_010": "NA",
    "DL_001": "FD2",
    "DL_007": "NA",
    "DL_009": "NA",
    "DL_043": "FD1"
}

var praPriorityDictionary = {
    IAA: 1,
    ISA: 2,
    NA: 3,
    FD1: 4,
    FD2: 5,
    FD4: 6,
    FP4P: 7,
    Other: 8
}


// Exports
module.exports = {
    productCode: productCode,
    productName: productName,
    productCategory: productCategory,
    productType: productType,
    productTypeDescription: productTypeDescription,

    praPriority: praPriority,

    data: {
        productCodes: Object.keys(productCodes)
    }
}
